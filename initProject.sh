
echo  $1


rm -rf src/main/submodule src/ale-base-backend src/base-account-nestjs/ .gitmodules

rm -rf .git

git init --initial-branch=main

git remote add origin $1

git add .

git commit -m "Initial commit"

git push -u origin main

git submodule add git@gitlab.com:ale-tech/share/ale-base-model.git src/main/submodule/ale-base-model

git submodule add git@gitlab.com:ale-tech/share/base-account-nestjs.git src/base-account-nestjs

git submodule add git@gitlab.com:ale-tech/share/ale-base-backend.git src/ale-base-backend

git submodule update --init --recursive 

git add .

git commit -m "update model"

git push