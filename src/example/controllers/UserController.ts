import { Controller } from '@nestjs/common';
import { BaseUserController } from '@BaseAccount/controllers/BaseUserController';
import { IBaseUserController } from '@CoreAccount/controller/IBaseUserController';
import { User } from '../model/User';
import { UserService } from '../services/UserService';
import { TokenJwtService } from '@BaseAccount/guard/TokenJwtService';

@Controller()
export class UserController extends BaseUserController<User> implements IBaseUserController<User> {
    constructor(private userService: UserService, tokenJwtService: TokenJwtService) {
        super(userService, tokenJwtService);
    }
}
