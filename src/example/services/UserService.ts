import { BaseUserService } from '@BaseAccount/services/BaseUserService';
import { BaseService } from '@BaseBackend/service/BaseService';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '../model/User';
import { UserRepository } from '../repository/UserRepository';

@Injectable()
export class UserService extends BaseUserService<User> {
    constructor(jwtService: JwtService, private userRepository: UserRepository) {
        super(jwtService, userRepository);
    }
}
