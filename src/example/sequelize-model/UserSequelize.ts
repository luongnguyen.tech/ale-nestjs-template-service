import { sequelizeFactory } from '@BaseBackend/utils/SequelizeFactory';
import { sequelizeConnector } from '@Config/SequelizeConnector';
import { ACCOUNT_SERVICE_NAME } from '@CoreAccount/index';
import Sequelize from 'sequelize';

export const userModel = sequelizeFactory<any>(sequelizeConnector, 'user', {
    accountId: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
            model: ACCOUNT_SERVICE_NAME.account,
        },
    },

    name: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
            isEmail: true,
        },
    },

    picture: {
        type: Sequelize.STRING,
    },
});
