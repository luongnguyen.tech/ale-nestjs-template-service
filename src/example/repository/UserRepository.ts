import { BaseRepository } from '@BaseBackend/decorator/BaseRepository';
import { sequelizeConnector } from '@Config/SequelizeConnector';
import { Injectable, OnModuleInit } from '@nestjs/common';
import { ModuleRef } from '@nestjs/core';
import { User } from '../model/User';
import { userModel } from '../sequelize-model/UserSequelize';

@Injectable()
export class UserRepository extends BaseRepository<User> implements OnModuleInit {
    constructor(private moduleRef: ModuleRef) {
        super(sequelizeConnector, userModel);
    }
    onModuleInit() {}
}
