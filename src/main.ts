import { HttpErrorFilter } from '@BaseBackend/decorator/HttpExceptionFilter';
import { routerHelper } from '@BaseBackend/helper/RouterHelper';
import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';
import { config } from 'dotenv';
import { MigrationHelper } from '@BaseBackend/migration/MigrationHelper';
import { appConfig } from '@Config/IConfig';
import { AppModule } from '@Config/app.module';

config();
const migrationPaths = [`dist/*/database/entities`, `dist/*/database/seeders`];

new MigrationHelper(appConfig.postgres).startMigration(migrationPaths).then(async (_) => {
    async function bootstrap() {
        const app = await NestFactory.create(AppModule, { cors: false });
        app.enableCors({
            origin: true,
            methods: '*',
            credentials: true,
            // exposedHeaders: ['Authorization'],
        });
        app.use(cookieParser());

        await app.listen(process.env.PORT || 6003);
        await routerHelper.syncAvailableRoutesToDB(app);
        app.useGlobalFilters(new HttpErrorFilter());
    }
    bootstrap();
});
