/* eslint-disable prettier/prettier */
import { Options } from 'sequelize';
import { config } from 'dotenv';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
config();

export interface IConfig {
    // todo : base Config
    postgres: Options;
    jwt: {
        secret: string;
        algorithm: string;
        maxExpires: number; // miliSecond
        timeRefresh: number; //miliSecond
    };
    accountConfig: {
        limitTimeOtp: number; //second
    };
    apiKey: {
        currentConvert: string;
    };
    web: {
        ecommerceUrl: string;
    };
    notification: {
        slack: {
            order: string;
        };
    };
    nodeEnv: string;
    email: {
        config: SMTPTransport.Options;
        emailTemplate: {};
    };
    googleAnalytics: {
        keyId: string;
    };
}

export const appConfig: IConfig = {
    postgres: {
        host: process.env.POSTGRES_HOST || 'localhost',
        port: Number(process.env.POSTGRES_PORT) || 5432,
        username: process.env.POSTGRES_USERNAME || 'aletech',
        password: process.env.POSTGRES_PASSWORD || '123456a@',
        database: process.env.POSTGRES_DATABASE || 'aletech',
        dialect: 'postgres',
        logging: false,
        define: {
            freezeTableName: true,
        },
    },
    accountConfig: {
        limitTimeOtp: 60 * 10,
    },

    jwt: {
        secret: 'ale123',
        algorithm: 'HS256',
        maxExpires: 1000 * 60 * 60 * 24 * 2,
        timeRefresh: 1000 * 60 * 60 * 10,
    },
    apiKey: {
        currentConvert: 'a05e13b0-58e1-11ec-94af-0d6417cacbb9',
    },
    web: {
        ecommerceUrl: process.env.WEB_ECOMMERCE_URL || 'https://ecommerce.afivn.com',
    },
    notification: {
        slack: {
            order: 'https://hooks.slack.com/services/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',
        },
    },
    nodeEnv: process.env.NODE_ENV,
    googleAnalytics: {
        keyId: '257684356',
        // 298007882
    },
    email: {
        config: {
            host: 'email-smtp.ap-southeast-1.amazonaws.com',
            port: 465,
            secure: true,
            auth: {
                user: 'AKIAV5T2TREZZA62XJVG',
                pass: 'BKrax+SrxEnrzccGyQjH93Ls8YEqf2XHB9+CcrGuILiw',
            },
        },
        emailTemplate: {},
    },
};

console.log(`------- Load config with env is ${process.env.NODE_ENV}-------`);
