import { SendEmailService } from '@BaseBackend/service/SendEmailService';
import { requirer } from '@Core/utils/Requirer';
import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ScheduleModule } from '@nestjs/schedule';
import { JwtAuthGuard } from 'src/base-account-nestjs/guard/JwtAuthGuard';
import { JwtStrategy } from 'src/base-account-nestjs/guard/JwtStrategy';
import { TokenJwtService } from 'src/base-account-nestjs/guard/TokenJwtService';
import { appConfig } from './IConfig';

@Module({
    imports: [
        // AccountModule,
        ScheduleModule.forRoot(),
        PassportModule,
        JwtModule.register({
            secret: appConfig.jwt.secret,
            signOptions: { expiresIn: `${appConfig.jwt.maxExpires / 1000}s` },
        }),
    ],
    controllers: [...requirer('dist/*/controllers/*')],
    providers: [
        ...requirer('dist/*/services/*'),
        ...requirer('dist/*/repository/*'),
        TokenJwtService,
        SendEmailService,
        JwtStrategy,
        JwtAuthGuard,
        { provide: 'APP_GUARD', useClass: JwtAuthGuard },
    ],
})
export class AppModule {}
