export interface Message {
    account: {
        register: {
            accountExisted: (username: string) => string;
        };
        login: {
            accountNotExisted: (username: string) => string;
            accountNotCorrect: (username: string) => string;
        };
    };
}

export const message: Message = {
    account: {
        register: {
            accountExisted: (username: string) => `Account ${username} already existed!`,
        },
        login: {
            accountNotExisted: (username: string) => `Account ${username} is not existed!`,
            accountNotCorrect: (username: string) => `Username/password is not correct`,
        },
    },
};
