import admin from 'firebase-admin';
import * as fs from 'fs';

const configJson = JSON.parse(fs.readFileSync('firebase-secrec.json').toString());

export const configAdminFirebase = admin.initializeApp({
    credential: admin.credential.cert({
        ...configJson,
    }),
});
