import { Sequelize } from 'sequelize';
import { appConfig } from './IConfig';

let { username, password, database } = appConfig.postgres;
export const sequelizeConnector = new Sequelize(database || '', username || '', password, appConfig.postgres);
